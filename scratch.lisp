
; (defun prove (theorem &key (axioms *axioms*))
;

(defvar *axioms* t)

(prove (theorem 

  (prove "(EXI (?X ?Y) (AND (MOTHER ?X ?Y)(NOT (MOTHER ?Y ?X))))")
  (prove "exi(X,Y, mother(X,Y) ^ ~mother(Y,X))")

