; Should these be symbols, lists, bool's, int's, or what?
(defvar a "a")
(defvar b "b")
(defvar c "c")

; �	on(a,a)		on(a,b)	�	on(a,c)
; �	on(b,a)	�	on(b,b)		on(b,c)
; �	on(c,a)	�	on(c,b)	�	on(c,c)

(defun on (x y)
  (cond
     ((and (equal x "a") (equal y "b")) t)
     ((and (equal x "b") (equal y "c")) t)
     (t nil)
  )
)

